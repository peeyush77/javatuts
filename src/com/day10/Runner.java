package com.day10;

public class Runner {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Loan l=new Loan();
		System.out.println(l.calculateEMI());
		
		CarLoan c=new CarLoan();
		System.out.println(c.calculateEMI());
		
		Loan cl=new CarLoan(); //Dynamic Polymorphism - allowed in run time
		System.out.println(cl.calculateEMI()); //Method overriding
		
		/*CarLoan lc=new Loan(); Not possible as parent class needs will be first loaded
								into the memory before the child class*/
	}

}
