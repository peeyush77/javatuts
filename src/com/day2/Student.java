package com.day2;

public class Student {
private String name;
private int rollNo;
private double marks1;
private double marks2;
private double marks3;

public double calculatePercent(){
	double totMarks=marks1+marks2+marks3;
	double percent=(totMarks/300)*100;
	return percent;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public int getRollNo() {
	return rollNo;
}

public void setRollNo(int rollNo) {
	this.rollNo = rollNo;
}

public double getMarks1() {
	return marks1;
}

public void setMarks1(double marks1) {
	this.marks1 = marks1;
}

public double getMarks2() {
	return marks2;
}

public void setMarks2(double marks2) {
	this.marks2 = marks2;
}

public double getMarks3() {
	return marks3;
}

public void setMarks3(double marks3) {
	this.marks3 = marks3;
}


}
