package com.day2;

public class ProductInventory {
	private String prodName;
	private String prodDet;
	private int prodPrice;
	
	public String getProdName() {
		return prodName;
	}
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}
	public String getProdDet() {
		return prodDet;
	}
	public void setProdDet(String prodDet) {
		this.prodDet = prodDet;
	}
	public int getProdPrice() {
		return prodPrice;
	}
	public void setProdPrice(int prodPrice) {
		this.prodPrice = prodPrice;
	}
	
}
