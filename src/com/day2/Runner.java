package com.day2;

public class Runner {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Student class
		Student s1=new Student();
		s1.setName("Peeyush");
		System.out.println(s1.getName());
		s1.setRollNo(328077);
		System.out.println(s1.getRollNo());
		s1.setMarks1(72.5);
		s1.setMarks2(67);
		s1.setMarks3(87.5);
		System.out.println(s1.calculatePercent());
		
		//Product Inventory class
		ProductInventory p=new ProductInventory();
		p.setProdName("Alpine Stars");
		p.setProdDet("Mesh type summer riding jacket");
		p.setProdPrice(15000);
		System.out.println(p.getProdName());
		System.out.println(p.getProdDet());
		System.out.println(p.getProdPrice());
	}

}
