package com.day7;

public class Person {
	private String name;
	private char gender;
	private String phoneNumber[];
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public char getGender() {
		return gender;
	}
	public void setGender(char gender) {
		this.gender = gender;
	}
	public void setPhoneNumber(String[] phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public void printArray() {
		for(int i=0;i<phoneNumber.length;i++) {
			System.out.println(phoneNumber[i]);
		}
	}
}
