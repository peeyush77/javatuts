package com.day9;

public class Shape {
	public double area(double radius) {
		double area=3.142*radius*radius;
		return area;
	}
	public double area(float side) {
		double area=side*side;
		return area;
	}
	public double area(double length,double breadth) {
		double area=length*breadth;
		return area;
	}
}

/*Method overloading: Here,methods with the same name are differentiated on the basis of
parameter types or the number of parameters.The 3 area() methods here have different
parameter types and also have different number of parameters*/
