package com.day9;

public class Loan {
	public double calcEMI(double principal,int tenure) {
		double rate=14.50;
		double Si=(principal*rate*tenure)/100;
		double TotalDue=principal+Si;
		double EMI=TotalDue/tenure;
		return EMI;
	}
	public double calcEMI(double principal,double rate,int tenure) {
		double Si=(principal*rate*tenure)/100;
		double totalDue=principal+Si;
		double EMI=totalDue/tenure;
		return EMI;
	}
}
