package com.day4;

public class ConditionalStatements {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int number=0;
		if(number>5)
//if statement says if the condition is true execute the next line
//next line can be in a code block or a statement
//if the condition is false, then it will execute the rest of the program	
		System.out.println("number is not greater than 5");
		System.out.println("Hi");
	}

}
