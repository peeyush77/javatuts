package com.day4;

public class Account {
private String name;
private char accountType;
private char gender;

public void createAccount() {
	if(validateGender()&&validateAccount())
		System.out.println("Account can be created");
	else
		System.out.println("Invalid details entered");
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public char getAccountType() {
	return accountType;
}
public void setAccountType(char accountType) {
	this.accountType = accountType;
}
public char getGender() {
	return gender;
}
public void setGender(char gender) {
	this.gender = gender;
}
private boolean validateGender() {
	if(gender=='M'||gender=='F') 
		return true;
	else 
		return false;
}
private boolean validateAccount() {
	if(accountType=='C'||accountType=='S') 
		return true;
	else 
		return false;
}
}
