package com.day4;

public class Runner {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Account a1=new Account();
		a1.setName("Peeyush");
		System.out.println(a1.getName());
		a1.setGender('X');
		a1.setAccountType('S');
		a1.createAccount();
		//Another Object
		Account a2=new Account();
		a2.setName("Pratyush");
		System.out.println(a2.getName());
		a2.setGender('M');
		a2.setAccountType('P');
		a2.createAccount();
		//Another Object
		Account a3=new Account();
		a3.setName("Pooja");
		System.out.println(a3.getName());
		a3.setGender('W');
		a3.setAccountType('Z');
		a3.createAccount();
		//Another Object
		Account a4=new Account();
		a4.setName("Nina");
		System.out.println(a4.getName());
		a4.setGender('F');
		a4.setAccountType('S');
		a4.createAccount();
	}

}
