package com.day3;

public class AreaCalculate {
	private int length;
	private int breadth;
	private int side;
	private double radius;
	
	public void area(int length,int breadth) {
		int rectangleArea=length*breadth;
		System.out.println("Area of the rectangle is "+rectangleArea);
	}
	public void area1(int side) {
		int squareArea=side*side;
		System.out.println("Area of the square is "+squareArea);
	}
	public void area(int radius) {
		double circleArea=3.142*radius;
		System.out.println("Area of the circle is "+circleArea);
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public int getBreadth() {
		return breadth;
	}
	public void setBreadth(int breadth) {
		this.breadth = breadth;
	}
	public int getSide() {
		return side;
	}
	public void setSide(int side) {
		this.side = side;
	}
	public double getRadius() {
		return radius;
	}
	public void setRadius(double radius) {
		this.radius = radius;
	}
	
}
