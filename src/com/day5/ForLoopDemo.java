package com.day5;

public class ForLoopDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int x = 2; 
        for(long y = 0, z = 4; x < 10 && y < 10; x++, y++)  
        { 
            System.out.print(y + " "); 
        } 
      
        System.out.println(x); 
	}
}
	/* Java program to illustrate 
	// redeclaring a variable 
	// in initialization block 
	public class Example3 
	{ 
		public static void main(String[] args) 
		{	 
			// x is integer 
			int x = 0; 
			
			// redeclaring x as long will not work 
			for(long y = 0, x = 1; x < 5; x++) 
			{ 
				System.out.print(x + " "); 
			} 
			
		} 
	} 
	 // x and y scope is only  
        // within for loop 
        for(int x = 0, y = 0; x < 3 && y < 3; x++, y++)  
        { 
            System.out.println(y + " "); 
        } 
      
        System.out.println(x); 
    } 
*/

