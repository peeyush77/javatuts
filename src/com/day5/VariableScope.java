package com.day5;

public class VariableScope { 
		static int x = 11; 
		private int y = 33; 
		public void method1(int x) 
		{ 
			VariableScope s = new VariableScope(); 
			this.x = 22; 
			y = 44; 

			System.out.println("VariableScope.x: " + VariableScope.x); 
			System.out.println("s.x: " + s.x); 
			System.out.println("s.y: " + s.y); 
			System.out.println("y: " + y); 
		} 

		public static void main(String args[]) 
		{ 
			VariableScope t = new VariableScope(); 
			t.method1(5); 
		} 
	} 


