package com.day5;

public class ContinueDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Print only Odd nos between a range using Loops & Continue keyword
		for(int i=0;i<10;i++) {
			if(i%2==0) 
				continue; /*When the condition is satisfied,the remaining body
						  of the loop won't run anymore*/
			System.out.print(i+" "); 
		}
  

	}

}
