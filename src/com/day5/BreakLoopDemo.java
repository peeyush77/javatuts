package com.day5;

public class BreakLoopDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Initially loop is set to run from 0-9
		for(int i=0;i<10;i++) {
			if(i==5) 
				break; // terminates loop when i is 5.
		
		System.out.println("i is: "+i);
			}
		System.out.println("Loop complete");
		}

}
