package com.day6;

public class Student {
	private String name;
	private String ID;
	private double marksObtained[]; // CS Math Science
	private double totalMarks;
	private double percentage;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public double[] getMarksObtained() {
		return marksObtained;
	}
	public void setMarksObtained(double[] marksObtained) {
		this.marksObtained = marksObtained;
	}
	public double getTotalMarks() {
		return totalMarks;
	}
	public double getPercentage() {
		return percentage;
	}
	public void CalTotalMarks(){
		double sum=0;
		for(int i=0;i<marksObtained.length;i++) {
			sum=sum+marksObtained[i];
		}
		totalMarks=sum;
	}
	public void CalPercent() {
		double percent=(totalMarks/(marksObtained.length*100))*100;
		percentage=percent;
	}
}
