package com.day6;

public class Runner {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Student s=new Student();
		s.setName("Name: Peeyush Padhi");
		System.out.println(s.getName());
		s.setID("Roll no.: 3563");
		System.out.println(s.getID());
		double[] marksScored= {95,81.5,85,94,92}; //Creating and initializing an array
		s.setMarksObtained(marksScored);
		s.CalTotalMarks();
		System.out.println("Total marks scored: "+s.getTotalMarks());
		s.CalPercent();
		System.out.println("Percentage scored: "+s.getPercentage());
		
	}

}
