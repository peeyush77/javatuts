package com.day6;

public class ArrayExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 
		//int intArray[]= {7,9,8,6,5};Array literals if already size and values are known	
		//Declaring & instantiating an array
		int intArray[]=new int[5];
		//Assigning values to array elements by new keyword
		intArray[0]=5453543;
		intArray[1]=7123322;
		intArray[2]=-94442;
		intArray[3]=325691343;
		intArray[4]=-44234545;
		//Accessing array elements using loop
		for(int i=0;i<intArray.length;i++) {
			System.out.println("Element of the array at "+i+" location is "+intArray[i]);
		}
		
	}

}
