package com.day6;
import java.io.*; 
class Emp { 
	  
    // static variable salary 
    public static double salary; 
    public static String name = "Harsh"; 
} 
public class StaticVariableDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// accessing static variable without object 
		        Emp.salary = 10000; 
		        System.out.println(Emp.name + "'s average salary: "
		                           + Emp.salary); 
		        Emp e=new Emp();
		        System.out.println(e.salary);
		        /*Here the object name is automatically replaced by class name
		        								to access the static variable*/
		        System.out.println(e.name);
		        
		    } 
}

